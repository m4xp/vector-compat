# README #

This is fork [library](https://github.com/wnafee/vector-compat). Added small fixes and new features [such as](http://stackoverflow.com/a/37621687/3094065).

# Use with Gradle #

To start, copy vector-compat-1.1.aar in your libs folder for module then configure gradle as below:

For project build.gradle


```
#!gradle

allprojects {
    repositories {
        jcenter()
        flatDir {
            dirs 'libs'
        }
    }
}
```


For module build.gradle


```
#!gradle

dependencies {
    compile 'com.wnafee.vector:vector-compat:1.1@aar'
}
```


PS: The library is compiled only on the old api 22 and with errors, but works everywhere, tested on api 16, 23, 25. Just use vector-compat-1.1.aar