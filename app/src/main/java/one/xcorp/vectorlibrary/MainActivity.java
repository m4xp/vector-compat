package one.xcorp.vectorlibrary;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.wnafee.vector.compat.DrawableCompat;
import com.wnafee.vector.compat.ResourcesCompat;
import com.wnafee.vector.compat.VectorDrawable;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    VectorDrawable.VGroup rotationGroup;

    int[] a = new int[]{10, -10, 10, -10, 10, -10, 0, 0};
    int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DrawableCompat drawable = ResourcesCompat.getDrawable(this, R.drawable.ic_play_to_pause);

        drawable.getVectorDrawable().setAllowCaching(false);
        rotationGroup = (VectorDrawable.VGroup)  drawable.getVectorDrawable().getTargetByName("rotationGroup");

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageDrawable(drawable);

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ((Animatable) imageView.getDrawable()).start();
                return true;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotationGroup.setTranslateX(a[index++]);
                imageView.invalidate();
            }
        });
    }
}